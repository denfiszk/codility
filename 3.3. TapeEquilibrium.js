// https://app.codility.com/demo/results/trainingPK2FQN-GWW/

/*

A non-empty array A consisting of N integers is given. Array A represents numbers on a tape.

Any integer P, such that 0 < P < N, splits this tape into two non-empty parts: A[0], A[1], ..., A[P − 1] and A[P], A[P + 1], ..., A[N − 1].

The difference between the two parts is the value of: |(A[0] + A[1] + ... + A[P − 1]) − (A[P] + A[P + 1] + ... + A[N − 1])|

In other words, it is the absolute difference between the sum of the first part and the sum of the second part.

For example, consider array A such that:

  A[0] = 3
  A[1] = 1
  A[2] = 2
  A[3] = 4
  A[4] = 3
We can split this tape in four places:

P = 1, difference = |3 − 10| = 7 
P = 2, difference = |4 − 9| = 5 
P = 3, difference = |6 − 7| = 1 
P = 4, difference = |10 − 3| = 7 
Write a function:

function solution(A);

that, given a non-empty array A of N integers, returns the minimal difference that can be achieved.

For example, given:

  A[0] = 3
  A[1] = 1
  A[2] = 2
  A[3] = 4
  A[4] = 3
the function should return 1, as explained above.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [2..100,000];
each element of array A is an integer within the range [−1,000..1,000].

*/

// Mi solución - 33% :(

function solution(A){
    var cut = 1;
    var min = 5000000;
    var end = A.length;
    var absolute = 0;
    
    while(cut <= end){
      var A1 = A.slice(0,cut);
      var A2 = A.slice(cut, end);
      
      let A1total = A1.reduce((a, b) => a + b, 0);
      let A2total = A2.reduce((a, b) => a + b, 0);
      
      absolute = Math.abs(A1total-A2total);
      
      if(absolute <= min){
        min = absolute;
      }
      
     cut++; 
      
    }
  
  return min;
  
}

// Solución tio random - 100%

function solution(A){
    var sumLeft = A[0];
    var sumRight = A.slice(1, A.length).reduce((e, sum) => { return e + sum;}, 0);
    var absolute = Math.abs(sumLeft - sumRight);
    var cut = 1;
  
    if (A.length === 2) return Math.abs(A[0] - A[1])
  
    for(cut; cut< A.length-2; cut++){
       sumLeft += A[cut];
        sumRight -= A[cut];   
        let tempDiff = Math.abs(sumLeft - sumRight);
      
      if(tempDiff <= absolute){
        absolute = tempDiff;
      }
      
    }
  
  return absolute;
  
}