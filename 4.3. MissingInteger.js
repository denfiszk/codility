// https://app.codility.com/demo/results/trainingKS86Y5-8S8/

/*

This is a demo task.

Write a function:

function solution(A);

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

Given A = [1, 2, 3], the function should return 4.

Given A = [−1, −3], the function should return 1.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000].

*/

// Mi solución - 66%

function solution(A){
  
  var sol = 1;
  var max = Math.max(...A);
  
  // console.log(max);
  
  for(var i = 1; i <= max+1; i++){
    if(A.indexOf(i) === -1){
      sol = i;
      break;
    }
  }

  return sol;
  
}