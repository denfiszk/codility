/*

An array A consisting of N integers is given. Rotation of the array means that each element is shifted right by one index, and the last element of the array is moved to the first place. For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7] (elements are shifted right by one index and 6 is moved to the first place).

The goal is to rotate array A K times; that is, each element of A will be shifted to the right K times.

Assume that the following declarations are given:

struct Results {
  int * A;
  int N; // Length of the array
};

Write a function:

struct Results solution(int A[], int N, int K);

that, given an array A consisting of N integers and an integer K, returns the array A rotated K times.

For example, given

    A = [3, 8, 9, 7, 6]
    K = 3
the function should return [9, 7, 6, 3, 8]. Three rotations were made:

    [3, 8, 9, 7, 6] -> [6, 3, 8, 9, 7]
    [6, 3, 8, 9, 7] -> [7, 6, 3, 8, 9]
    [7, 6, 3, 8, 9] -> [9, 7, 6, 3, 8]
For another example, given

    A = [0, 0, 0]
    K = 1
the function should return [0, 0, 0]

Given

    A = [1, 2, 3, 4]
    K = 4
the function should return [1, 2, 3, 4]

Assume that:

N and K are integers within the range [0..100];
each element of array A is an integer within the range [−1,000..1,000].
In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.

*/

// MI SOLUCIÓN - 50% 
function solution(A, K){
  var result = A;
  
  if (A.length > 0) {
    var saver = [];
    var i = 0;
    var long = A.length-1;
  
    // console.log("Given Array: " + A)
    
    while (i < K){
      saver[i] = A[long-i];
      i++;
    }
  
    i = 0;
  
    while(i < K){
      A.pop();
      i++;
    }
  
    saver = saver.reverse();
    
    // console.log(saver);
  
    result = saver.concat(A);
    // console.log("Resulting Array: " + result, "Displacement: " + K);
  
  }
  return result;
  
}

// SOLUCIÓN DE EDU - 100%

/*

 function solution(A, K) {
     // write your code in JavaScript (Node.js 8.9.4)
     const arrLen = A.length
     const mod = K % arrLen
     if(arrLen === K || arrLen === 1 || mod === 0) return A
     return [...A.slice(arrLen - mod), ...A.slice(0, arrLen - mod)]
 }

*/