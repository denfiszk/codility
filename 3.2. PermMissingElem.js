// https://app.codility.com/demo/results/trainingWN35XU-EJQ/
// https://app.codility.com/demo/results/training36466R-UGF/

/*

An array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.

Your goal is to find that missing element.

Write a function:

function solution(A);

that, given an array A, returns the value of the missing element.

For example, given array A such that:

  A[0] = 2
  A[1] = 3
  A[2] = 1
  A[3] = 5
the function should return 4, as it is the missing element.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [0..100,000];
the elements of A are all distinct;
each element of array A is an integer within the range [1..(N + 1)].

*/

// Mi solución - 100% (idea de edu)

function solution(A){

var key = A.length;
var totalArr = 0;
var i = 0;
var total = (key+1)*((key+2))/2;

while(i < key){
    
    totalArr += A[i];
    i++;
    
}
  
  // console.log(total, totalArr, key)

var result = total - totalArr;

// if(key === 0){
//     result = 2;
// }

return Math.abs(result);

}